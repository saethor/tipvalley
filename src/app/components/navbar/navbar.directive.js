export function NavbarDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/navbar/navbar.html',
    scope: {
        creationDate: '='
    },
    controller: NavbarController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class NavbarController {
  constructor ($log, $http, $auth, $state, LeaguesService, userService, api) {
    'ngInject';
    this.api = api;

    this.LeaguesService = LeaguesService;
    this.$http = $http;
    this.$log = $log;
    this.$auth = $auth;
    this.$state = $state;
    this.userService = userService;

    this.getLeagues();
    this.getUser();
  }

  /**
   * Gets information about logged in user
   *
   * @returns {*}
   */
  getUser() {
    const user = this.userService;

    user.get()
      .then(responde => {
        this.user = responde.user;
      })
    ;
  }

  /**
   * Gets all leagues user is in
   *
   * @returns {Array}
   */
  getLeagues() {
    if ( ! this.$auth.isAuthenticated() )
      return this.leagues = [];

    this.LeaguesService.get()
      .then( leagues => {
        this.leagues = leagues.leagues;
      })

  }

  /**
   * Logs out user
   */
  logout() {
    this.$auth.logout().then(() => {
      localStorage.removeItem('user');
      this.$state.go('login', {});
    })
  }

  /**
   * Checks if there is a authenticated user
   */
  isAuthenticated() {
    return this.$auth.isAuthenticated();
  }
}
