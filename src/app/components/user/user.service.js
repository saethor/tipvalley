export class UserService {

  /**
   * Constructor for UserService
   * @param $http
   * @param api
   */
  constructor($http, api) {
    'ngInject';
    this.$http = $http;
    this.api = api + '/users';
  }

  /**
   * Returns a logged in user
   */
  get() {
    const $http = this.$http;
    const api = this.api;

    return $http.get(api)
      .then(user => user.data)
      .catch(error => error.data)
      ;
  }

  /**
   * Creates a new user
   *
   * @param first_name
   * @param last_name
   * @param email
   * @param password
   * @param password_confirmation
   */
  create(first_name, last_name, email, password, password_confirmation) {
    const $http = this.$http;
    const api = this.api;

    return $http.post(api, {first_name, last_name, email, password, password_confirmation})
      .then(responde => responde.data, error => error.data)
      .catch(error => error.data)
      ;
  }

}
