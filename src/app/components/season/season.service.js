export class SeasonService {
  constructor($http, api) {
    'ngInject';

    this.$http = $http;
    this.api = api + '/seasons';
  }

  get() {
    const $http = this.$http;
    const api = this.api;

    return $http.get(api)
      .then( responde => responde.data );
  }
}
