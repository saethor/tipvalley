export class FixtureService {

  constructor($http, $log, api) {
    'ngInject';

    this.$http = $http;
    this.$log = $log;
    this.APIHOST = api + '/leagues/';
  }

  getFixtures(leagueId) {
    return this.$http.get(this.APIHOST + leagueId + '/fixtures')
      .then( response => response.data)
      .catch( error => this.$log = error )
  }

  postFixture(leagueId, id, results) {
    this.$http.post(this.APIHOST + leagueId + '/fixtures', {"fixture_id": id, results})
      .then(
        message => this.$log.log(message),
        error => this.$log.error(error)
      );
  }
}
