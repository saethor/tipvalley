export function FixtureDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/leagues/fixture/fixture.html',
    scope: {},
    controller: FixtureController,
    controllerAs: 'games',
    bindToController: true
  };

  return directive;
}

class FixtureController {
  constructor($log, $stateParams, fixtures, _) {
    'ngInject';
    this.$log = $log;
    this.fixture = fixtures;
    this.leagues = {};
    this._ = _;

    this.points = 0;
    this.failed = 0;

    this.leagueId = $stateParams.id;

    fixtures.getFixtures($stateParams.id).then((results) => {
      this.fixtures = results;
      this.filterByMatchDays(this.fixtures);
    });
  }

  /**
   * Groups together matches from the same match day
   *
   * @param fixtures
   */
  filterByMatchDays(fixtures) {
    const _ = this._;

    _.forEach(fixtures, (matches, league) => {
      this.leagues[league] = _.groupBy(matches, function (match) {
        return match.matchday;
      })
    });
  }

  /**
   * Calculates the score and returns the appropriate class name
   *
   * @param tip
   * @param results
   * @returns {*}
   */
  calculateScore(tip, results) {
    if (results.goalsHomeTeam === null)
      return;

    if (results.goalsHomeTeam > results.goalsAwayTeam && tip === '1') {
      this.points++;
      return 'success';
    }
    else if (results.goalsHomeTeam === results.goalsAwayTeam && tip === 'X') {
      this.points++;
      return 'success';
    }
    else if (results.goalsHomeTeam < results.goalsAwayTeam && tip === '2') {
      this.points++;
      return 'success';
    }
    else if (results.goalsHomeTeam >= 0) {
      this.failed++;
      return 'danger';
    }
  }

  /**
   * Creates a new tip
   *
   * @param leagueId
   * @param tipId
   * @param results
   */
  tip(leagueId, tipId, results)
  {
    this.fixture.postFixture(leagueId, tipId, results);
  }
}
