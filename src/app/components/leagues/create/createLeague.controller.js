export class CreateLeagueController {

  /**
   * Constructor and injector for creating a new league
   * @param $log
   * @param $state
   * @param LeaguesService
   * @param seasonService
   */
  constructor($log, $state, LeaguesService, seasonService) {
    'ngInject';
    this.LeaguesService = LeaguesService;
    this.seasonService = seasonService;
    this.$log = $log;
    this.$state = $state;
    this.players = [];
    this.loading = false;

    this.seasons();
  }

  /**
   * Sends information to LeagueServices about the new league.
   */
  createLeague() {
    this.loading = true;
    this.LeaguesService.create( this.name, this.description, this.season_id, ...this.players )
      .then( response => {
        this.status = (response.message) ? response.message : response.data;
        this.name = '';
        this.description = '';
        this.season_id = '';
        this.players = [];
        this.loading = false;

        let form  = angular.element('#createLeague');

        this.reset(form);
      })
  }

  seasons() {
    const season = this.seasonService;
    season.get()
     .then(responde => {
       this.seasons = responde.seasons;
     });
  }

  /**
   * Creates a array of players
   *
   * @returns {string}
   */
  addPlayer() {
    this.players.push(this.player);
    this.player = '';
  }

  /**
   * http://stackoverflow.com/questions/31010096/how-do-i-reset-a-form-including-removing-all-validation-errors
   */
  reset(form) {
    // Each control (input, select, textarea, etc) gets added as a property of the form.
    // The form has other built-in properties as well. However it's easy to filter those out,
    // because the Angular team has chosen to prefix each one with a dollar sign.
    // So, we just avoid those properties that begin with a dollar sign.
    let controlNames = Object.keys(form).filter(key => key.indexOf('$') !== 0);

    // Set each control back to undefined. This is the only way to clear validation messages.
    // Calling `form.$setPristine()` won't do it (even though you wish it would).
    for (let name of controlNames) {
      let control = form[name];
      control.$setViewValue(undefined);
    }

    form.$setPristine();
    form.$setUntouched();
  }
}
