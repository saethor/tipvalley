export class LeagueController {
  constructor($log, $stateParams, LeaguesService) {
    'ngInject';
    this.loading = false;
    this.leagueService = LeaguesService;
    this.$log = $log;
    this.id = $stateParams.id;
  }


  addPlayer() {
    this.loading = true;

    this.leagueService.addUser(this.id, this.player)
      .then( message => {
        this.loading = false;
        this.$log.log(message);
        this.player = '';
      }, error => {
        this.$log.log(error);
        this.loading = false;
        this.player = '';
      } );
  }
}
