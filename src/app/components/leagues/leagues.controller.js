export class LeaguesController {

  /**
   * Constructor for Leagues
   *
   * @param LeaguesService
   * @param $log
   * @param $state
   */
  constructor (LeaguesService, $log, $state) {
    'ngInject';

    this.$log = $log;
    this.LeaguesService = LeaguesService;
    this.$state = $state;

    this.getLeagues();
  }


  addUser() {
    const leagueService = this.LeaguesService;

    leagueService.addUser(this.email);
  }


  /**
   * Gets all the leagues that user is in from LeaguesService.
   */
  getLeagues() {
    const leaguesService = this.LeaguesService;

    leaguesService.get()
      .then(response => {
        this.leagues = response.leagues;
      }, error => {
        this.$log.error(error);
      });
  }
}

