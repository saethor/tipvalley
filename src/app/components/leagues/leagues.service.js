export class LeaguesService {

  /**
   * Constructor for Leagues.
   *
   * @param $log
   * @param $http
   * @param $q
   * @param api
   * @param $state
   */
  constructor($log, $http, $q, api, $state) {
    'ngInject';

    this.$state = $state;
    this.$log = $log;
    this.$http = $http;
    this.$q = $q;
    this.APIHOST = api + '/leagues';
    this.update = false;
  }

  /**
   * Returns all or a single leagues based on if id is given or not
   *
   * @param id
   * @returns {*|Promise.<T>}
   */
  get(id) {
    const leagueId = (id) ? id : null;
    const $http = this.$http;
    const api = (leagueId) ? this.APIHOST + '/' + leagueId : this.APIHOST;

    this.update = false;

    return $http.get(api)
      .then( response => response.data )
      .catch( error => error );
  }

  /**
   * Creates a new league
   *
   * @param name
   * @param description
   * @param season_id
   * @param players
   * @returns {*}
   */
  create(name, description, season_id, ...players) {
    const $http = this.$http;
    const api = this.APIHOST;


    if (name === angular.isUndefined || description === angular.isUndefined) {
      return this.$log.log('error');
    }

    this.update = true;

    return $http.post(api, {name, description, season_id, players})
      .then( response => response.data )
      .catch( error => error );
  }

  /**
   * Adds a new player to a league
   *
   * @param leagueId
   * @param user
   * @returns {*|Promise.<T>}
   */
  addUser(leagueId, user) {
    const $http = this.$http;
    const defer = this.$q.defer();
    const league = parseInt(leagueId);
    const api = this.APIHOST + '/' + league + '/users';

    $http.post(api, {user})
      .then( response => {
        defer.resolve(response.data);
      }, error => {
        defer.reject(error.data);
      });

    return defer.promise;
  }

  /**
   * Adds a new season to a league
   *
   * @param id
   * @param seasonId
   */
  addSeason(id, seasonId) {
    const $http = this.$http;
    const $log = this.$log;
    const leagueId = parseInt(id);
    const api = this.APIHOST + '/leagues/' + leagueId + '/seasons';

    $http.post(api, {'season_id': seasonId})
      .then( message => {
        $log.log(message.data);
      })
      .catch(error => {
        $log.log(error);
      });
  }
}
