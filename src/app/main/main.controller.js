export class MainController {
  constructor (LeaguesService, $log, $state) {
    'ngInject';

    this.$log = $log;
    this.LeaguesServicee = LeaguesService;
    this.$state = $state;
    this.getLeagues();
  }

  getLeagues() {
    this.LeaguesService.getLeagues()
      .then(response => {
        this.leagues = response.leagues;
      }, error => {
        this.$log.error(error);
      });
  }

  league($id) {
     this.$state.go('leagues.details', $id);
  }
}
