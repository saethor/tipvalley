export function routerConfig ($stateProvider, $urlRouterProvider, $locationProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);

  $stateProvider

    .state('main', {
      url: '/',
      views: {
        'header': {
          templateUrl: 'app/components/header/header.html'
        },
        'main': {
          templateUrl: 'app/main/main.html'
        }
      }
    })
    .state('leagues', {
      abstract: true,
      views: {
        'header': {
          templateUrl: 'app/components/header/header.html'
        },
        'main': {
          templateUrl: 'app/components/leagues/leagues.html'
        }
      },
      controller: 'LeaguesController',
      controllerAs: 'vm'
    })
    .state('leagues.show', {
      url: '/leagues',
      views: {
        'create': {
          templateUrl: 'app/components/leagues/create/createLeague.html',
          controller: 'CreateLeagueController',
          controllerAs: 'vm'
        }
      }
    })
    .state('league', {
      url: '/leagues/:id',
      views: {
        'main': {
          'templateUrl': 'app/components/leagues/league/league.html'
        }
      },
      controller: 'LeagueController',
      controllerAs: 'league'
    })
    .state('login', {
      url: '/login',
      views: {
        'main': {
          templateUrl: 'app/login/login.html',
          controller: 'LoginController',
          controllerAs: 'login'
        }
      }
    })
    .state('signup', {
      url: '/login/register',
      views: {
        'main': {
          templateUrl: 'app/login/register/register.html',
          controller: 'RegisterController',
          controllerAs: 'register'
        }
      }
    })
  ;

  $urlRouterProvider.otherwise('/');
}
