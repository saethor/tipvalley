import * as _ from '../../node_modules/lodash/';
import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { LoginController } from './login/login.controller';
import { RegisterController } from './login/register/register.controller';
import { LeaguesService } from '../app/components/leagues/leagues.service';
import { FixtureService } from '../app/components/leagues/fixture/fixture.service';
import { FixtureDirective } from '../app/components/leagues/fixture/fixture.directive';
import { NavbarDirective } from '../app/components/navbar/navbar.directive';

import { UserService } from '../app/components/user/user.service';
import { SeasonService } from '../app/components/season/season.service';

import { LeaguesController } from '../app/components/leagues/leagues.controller';
import { LeagueController } from '../app/components/leagues/league/league.controller';

import { CreateLeagueController } from '../app/components/leagues/create/createLeague.controller';

angular.module('tipvalley', ['ui.router', 'satellizer', 'ngMessages', 'ui.select' ])
  .constant('_', _)
  .constant('api', 'http://api.tipvalley.com/beta')
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service('LeaguesService', LeaguesService)
  .service('fixtures', FixtureService)
  .service('userService', UserService)
  .service('seasonService', SeasonService)
  .controller('MainController', MainController)
  .controller('LoginController', LoginController)
  .controller('RegisterController', RegisterController)
  .controller('LeaguesController', LeaguesController)
  .controller('LeagueController', LeagueController)
  .controller('CreateLeagueController', CreateLeagueController)
  .directive('fixture', FixtureDirective)
  .directive('navbar', NavbarDirective)
;
