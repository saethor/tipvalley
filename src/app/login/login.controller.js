export class LoginController{
  constructor($auth, $state, $log) {
    'ngInject';

    this.$auth = $auth;
    this.$state = $state;
    this.$log = $log
  }

  login() {
    var credentials = {
      email: this.email,
      password: this.password
    };

    this.$auth.login(credentials)
      .then(() => {
        this.error = undefined;
        this.$state.go('main', {});
      })
      .catch(error => {
        this.error = error;
      });
  }
}
