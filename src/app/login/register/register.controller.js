export class RegisterController {
  constructor($log, $auth, $state, $http, api, userService) {
    'ngInject';
    this.$log = $log;
    this.$auth = $auth;
    this.$state = $state;
    this.$http = $http;
    this.api = api + "/users";
    this.user = userService;
    this.loading = false;
  }

  /**
   * Sends information's to userService that's registers a new user
   */
  register() {
    const user = this.user;
    const $state = this.$state;

    this.loading = true;

    user.create(this.firstName, this.lastName, this.email, this.password, this.passwordAgain)
      .then( responde => {
        if (responde.status_code)
          return $state.go('login');

        this.error = responde;
        this.loading = false;
      })
      .catch(error => error)
    ;
  }
}
