export function runBlock ($log, $rootScope, $state) {
  'ngInject';
  let on = $rootScope.$on('$stateChangeStart', function(event, toState) {
    var user = angular.fromJson(localStorage.getItem('user'));

    if(user) {
      $rootScope.authenticated = true;
      $rootScope.currentUser = user;
      if(toState.name === "login") {
        event.preventDefault();
        $state.go('home');
      }
    }

  });
  $log.debug('runBlock end');
}
