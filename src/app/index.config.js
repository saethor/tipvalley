export function config ($logProvider, $stateProvider, $authProvider, $httpProvider, $provide, api) {
  'ngInject';
  const APIHOST = api + '/users/authenticate';
  // Enable log
  $logProvider.debugEnabled(true);
  $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
  $httpProvider.interceptors.push('redirectWhenLoggedOut');
  $authProvider.loginUrl = APIHOST;

  function redirectWhenLoggedOut($q, $injector) {
    return {
      responseError: function(rejection) {
        var $state = $injector.get('$state');
        var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

        angular.forEach(rejectionReasons, function(value) {
          if(rejection.data.error === value) {
            localStorage.removeItem('user');

            $state.go('login');
          }
        });

        return $q.reject(rejection);
      }
    };
  }
}
